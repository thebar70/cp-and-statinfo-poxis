#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>


# define DT_DIR DT_DIR

void listar_recursivo(char *ruta );
void listar(char *ruta);
int cantidadSub_directorios(char *dir);

typedef struct struc_sub_directorios
{
    char * ruta_subdir;
} struc_sub_directorios;
 
void listar(char * ruta){
    struct stat fileStat;
    stat(ruta, &fileStat);
    if((fileStat.st_mode & S_IFMT) == S_IFDIR)
        printf("\tDirectorio %s   %d bytes\n",ruta,fileStat.st_size);
    else
        printf("\tArchivo %s   %d bytes\n",ruta,fileStat.st_size);
    
 }
//Implementacion de Funciones
void listar_recursivo(char *ruta){
    
    struct dirent *elemento;
    DIR * dir;
    dir=opendir(ruta);
    char *nuevo_dir;
    int pos=0;
    
    /*Obetemos la cantidad de subDirectorios para separa memoria*/
    struct struc_sub_directorios *sub_directorios;
    int cantidad=cantidadSub_directorios(ruta);
    sub_directorios=(struct struc_sub_directorios *) malloc(cantidad*sizeof(struc_sub_directorios));
    // printf("Valor de cantidad: %d",cantidad);
    struct stat fileStat;
    stat(ruta, &fileStat);
    printf("\n\t%s  %d bytes\n",ruta,fileStat.st_size);
    int len;
    while((elemento = readdir(dir)) != NULL){
        if( !strcmp(elemento->d_name, ".")) continue;
        if( !strcmp(elemento->d_name, "..")) continue;
        
      //  if((fileStat.st_mode & S_IFMT) == S_IFDIR){
          if(elemento->d_type==DT_DIR){
            len = strlen(elemento->d_name) +strlen(ruta) +2;
            nuevo_dir=(char *) malloc(  len );
            strcpy(nuevo_dir,ruta);
            strcat(nuevo_dir,elemento->d_name);
            strcat(nuevo_dir,"/");
            stat(nuevo_dir, &fileStat);
            printf("\t\t\033[0;34m\tDIR: %s   %d bytes\n",elemento->d_name,fileStat.st_size);
            printf("\033[0m");
            sub_directorios[pos].ruta_subdir = (char*)malloc(len);
            strcpy(sub_directorios[pos].ruta_subdir,nuevo_dir);
            pos++;
       
        }else{
            len = strlen(elemento->d_name) +strlen(ruta) +2;
            nuevo_dir=(char *) malloc(  len );
            strcpy(nuevo_dir,ruta);
            strcat(nuevo_dir,elemento->d_name);
            stat(nuevo_dir, &fileStat);
            printf("\t\t%s  %d bytes\n",elemento->d_name,fileStat.st_size);
          
        }
       
        
        
    }
    closedir(dir);
    printf("\n");
    for(int i = 0; i < pos; i++){
        
        listar_recursivo(sub_directorios[i].ruta_subdir);
        free(sub_directorios[i].ruta_subdir);
    }
    free(sub_directorios);
    
}

int cantidadSub_directorios(char *ruta){
    int cantidad=0;
    DIR *dir;
    dir=opendir(ruta);
    struct dirent *elemento;
    while((elemento = readdir(dir)) != NULL){
        if( !strcmp(elemento->d_name, ".")) continue;
        if( !strcmp(elemento->d_name, "..")) continue;
         if(elemento->d_type == DT_DIR)
            cantidad++;
    }
    closedir(dir);
    
    return cantidad;


}

int main(int argc, char * argv[]){
    
    if(argv[1]==NULL){
        listar(getenv("PWD"));
    }else if (strcmp(argv[1],"-r")==0){
        printf("es recursivo\n");
        listar_recursivo(argv[2]);
    }else{
        listar(argv[1]);
        
        
       
    }
    exit(EXIT_SUCCESS);
}