#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

void copiar(char *, char *, char *);

void copiar(char *file_origen, char *file_destino, char *ruta_salida){
    char buffer[512]; //Buffer de lectura del archivo de origen
    int orige_read,destino_read; //Se almacenan el valor devuelto por read de los archivos de origen y destinno
    int bytes; //Indicara el numero de bytes a leer y escribir
    
    orige_read = open(file_origen, O_RDONLY);

    if(orige_read == -1){ //Se valida si se logro abrir correctamente el archivo de origen
        perror("Error al abrir el archivo de origen.\n");
        exit(1);
    }
    if(ruta_salida!=NULL){
        char * ruta;
        ruta=(char*)malloc(strlen(ruta_salida)+strlen(file_destino)+1);
        strcpy(ruta,ruta_salida);
        strcat(ruta,file_destino);
        FILE *archivo;
	    archivo = fopen(ruta,"a");
        fclose(archivo);
        destino_read = open(ruta, O_CREAT | O_WRONLY, 0660);//Se abre el archivo de destino
    }else{
        destino_read = open(file_destino, O_CREAT | O_WRONLY, 0660);//Se abre el archivo de destino
    }
    if(destino_read == -1){
            perror("Error al abrir el archivo de destino.\n");
            close(orige_read); //Cerrarmos el archivo de origen en caso de error
            exit(2);
    }
    //Se realiza la escritura de a bloques de 512 bytes
        while((bytes = read(orige_read, buffer, 512)) > 0){
            write(destino_read, buffer, bytes);
        }
    

    /* Close both source and target files. */
    close(orige_read);
    close(destino_read);
    printf("Se copio el archivo correctamente\n");

}
int main(int argc, char *argv[]){
    if(argc>2){
        if(argc==3){
            copiar(argv[1],argv[2],NULL);
        }else{
            copiar(argv[1],argv[2],argv[3]);
        }
    }else{
        printf("Faltan Parametros por ingresar");
        exit(EXIT_FAILURE);
    }
   
}